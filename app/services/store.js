var Todo = (function () {
    function Todo(title, dueTime) {
        this.completed = false;
        this.editing = false;
        this.title = title.trim();
        this.dueTime = dueTime;
    }
    Object.defineProperty(Todo.prototype, "title", {
        get: function () {
            return this._title;
        },
        set: function (value) {
            this._title = value.trim();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Todo.prototype, "dueTime", {
        get: function () {
            return this._dueTime;
        },
        set: function (value) {
            this._dueTime = value;
        },
        enumerable: true,
        configurable: true
    });
    return Todo;
})();
exports.Todo = Todo;
var TodoStore = (function () {
    function TodoStore() {
        var persistedTodos = JSON.parse(localStorage.getItem('angular2-todos') || '[]');
        // Normalize back into classes
        this.todos = persistedTodos.map(function (todo) {
            var ret = new Todo(todo._title, todo._dueTime);
            ret.completed = todo.completed;
            return ret;
        });
    }
    TodoStore.prototype.updateStore = function () {
        localStorage.setItem('angular2-todos', JSON.stringify(this.todos));
    };
    TodoStore.prototype.getWithCompleted = function (completed) {
        return this.todos.filter(function (todo) { return todo.completed === completed; });
    };
    TodoStore.prototype.allCompleted = function () {
        return this.todos.length === this.getCompleted().length;
    };
    TodoStore.prototype.setAllTo = function (completed) {
        this.todos.forEach(function (t) { return t.completed = completed; });
        this.updateStore();
    };
    TodoStore.prototype.removeCompleted = function () {
        this.todos = this.getWithCompleted(false);
        this.updateStore();
    };
    TodoStore.prototype.getRemaining = function () {
        return this.getWithCompleted(false);
    };
    TodoStore.prototype.getCompleted = function () {
        return this.getWithCompleted(true);
    };
    TodoStore.prototype.toggleCompletion = function (todo) {
        todo.completed = !todo.completed;
        this.updateStore();
    };
    TodoStore.prototype.remove = function (todo) {
        this.todos.splice(this.todos.indexOf(todo), 1);
        this.updateStore();
    };
    TodoStore.prototype.add = function (title, dueTime) {
        this.todos.push(new Todo(title, dueTime));
        this.updateStore();
    };
    TodoStore.prototype.changeDueTime = function (todo, value) {
        this.todos[this.todos.indexOf(todo)]['dueTime'] = value;
        this.updateStore();
    };
    TodoStore.prototype.moveDown = function (todo) {
        if (this.todos[this.todos.indexOf(todo) + 1]) {
            var next = this.todos[this.todos.indexOf(todo) + 1];
            this.todos[this.todos.indexOf(todo) + 1] = this.todos[this.todos.indexOf(todo)];
            this.todos[this.todos.indexOf(todo)] = next;
            this.updateStore();
        }
    };
    TodoStore.prototype.moveUp = function (todo) {
        if (this.todos[this.todos.indexOf(todo) - 1]) {
            var actuall = this.todos[this.todos.indexOf(todo)];
            var prev = this.todos[this.todos.indexOf(todo) - 1];
            this.todos[this.todos.indexOf(todo) - 1] = actuall;
            this.todos[this.todos.indexOf(todo) + 1] = prev;
        }
    };
    return TodoStore;
})();
exports.TodoStore = TodoStore;
//# sourceMappingURL=store.js.map