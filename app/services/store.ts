export class Todo {

	completed: Boolean;
	editing: Boolean;

    private _title: String;
    private _dueTime: String;

    get title() {
        return this._title;
    }
    set title(value: String) {
        this._title = value.trim();
    }

    get dueTime() {
        return this._dueTime;
    }
    set dueTime(value: String) {
        this._dueTime = value;
    }

	constructor(title: String, dueTime: String) {
		this.completed = false;
		this.editing = false;
        this.title = title.trim();
        this.dueTime = dueTime;
	}
}

export class TodoStore {

	todos: Array<Todo>;

	constructor() {

		let persistedTodos = JSON.parse(localStorage.getItem('angular2-todos') || '[]');
		// Normalize back into classes
		this.todos = persistedTodos.map( (todo: {_title: String, _dueTime: String, completed: Boolean}) => {
			let ret = new Todo(todo._title, todo._dueTime);
			ret.completed = todo.completed;
			return ret;
		});
	}

	private updateStore() {
		localStorage.setItem('angular2-todos', JSON.stringify(this.todos));
	}

	private getWithCompleted(completed: Boolean) {
		return this.todos.filter((todo: Todo) => todo.completed === completed);
	}

	allCompleted() {
		return this.todos.length === this.getCompleted().length;
	}

	setAllTo(completed: Boolean) {
		this.todos.forEach((t: Todo) => t.completed = completed);
		this.updateStore();
	}

	removeCompleted() {
		this.todos = this.getWithCompleted(false);
		this.updateStore();
	}

	getRemaining() {
		return this.getWithCompleted(false);
	}

	getCompleted() {
		return this.getWithCompleted(true);
	}

	toggleCompletion(todo: Todo) {
		todo.completed = !todo.completed;
		this.updateStore();
	}

	remove(todo: Todo) {
		this.todos.splice(this.todos.indexOf(todo), 1);
		this.updateStore();
	}

	add(title: String, dueTime: String) {
		this.todos.push(new Todo(title, dueTime));
		this.updateStore();
	}

    changeDueTime(todo: Todo, value:any) {
	    this.todos[this.todos.indexOf(todo)]['dueTime'] = value;
        this.updateStore();
    }

    moveDown(todo : Todo){
        if(this.todos[this.todos.indexOf(todo) + 1]){
            const next = this.todos[this.todos.indexOf(todo) + 1];
            this.todos[this.todos.indexOf(todo) + 1] = this.todos[this.todos.indexOf(todo)];
            this.todos[this.todos.indexOf(todo)] = next;
            this.updateStore();
        }
    }

    moveUp(todo : Todo){
        if(this.todos[this.todos.indexOf(todo) - 1]){
            const actuall = this.todos[this.todos.indexOf(todo)];
            const prev = this.todos[this.todos.indexOf(todo) - 1];
            this.todos[this.todos.indexOf(todo) - 1] =  actuall;
            this.todos[this.todos.indexOf(todo) + 1] = prev;
        }
    }

}
