"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var GoogleSigninComponent = (function () {
    function GoogleSigninComponent(element) {
        this.element = element;
        this.clientId = '409167539692-4eqnaq2jd1itl211gsgh3m2k7i02aefa.apps.googleusercontent.com';
        this.scope = [
            'profile',
            'email',
            'https://www.googleapis.com/auth/plus.me',
            'https://www.googleapis.com/auth/contacts.readonly',
            'https://www.googleapis.com/auth/admin.directory.user.readonly'
        ].join(' ');
        console.log('ElementRef: ', this.element);
    }
    GoogleSigninComponent.prototype.googleInit = function () {
        var that = this;
        gapi.load('auth2', function () {
            that.auth2 = gapi.auth2.init({
                client_id: that.clientId,
                cookiepolicy: 'single_host_origin',
                scope: that.scope
            });
            that.attachSignin(that.element.nativeElement.firstChild);
        });
    };
    GoogleSigninComponent.prototype.attachSignin = function (element) {
        var that = this;
        this.auth2.attachClickHandler(element, {}, function (googleUser) {
            var profile = googleUser.getBasicProfile();
            console.log('Token || ' + googleUser.getAuthResponse().id_token);
            console.log('ID: ' + profile.getId());
            console.log('Name: ' + profile.getName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail());
            //YOUR CODE HERE
        }, function (error) {
            console.log(JSON.stringify(error, undefined, 2));
        });
    };
    GoogleSigninComponent.prototype.ngAfterViewInit = function () {
        this.googleInit();
    };
    GoogleSigninComponent = __decorate([
        core_1.Component({
            selector: 'google-signin',
            template: '<button id="googleBtn">Google Sign-In</button>'
        })
    ], GoogleSigninComponent);
    return GoogleSigninComponent;
}());
exports.GoogleSigninComponent = GoogleSigninComponent;
var AppComponent = (function () {
    function AppComponent() {
        this.title = "Ajinkya's Google SignIn button";
        this.angularVersion = 'latest stable';
        console.clear();
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "{{title}}\n    <google-signin></google-signin>\n    <footer>Angular version: {{angularVersion}}</footer>"
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
