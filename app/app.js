var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('angular2/core');
var store_1 = require('./services/store');
var TodoApp = (function () {
    function TodoApp(todoStore) {
        this.todoStore = todoStore;
        this.levels = [
            { index: 'I', value: "Incomplete" },
            { index: 'DS', value: "Due Soon" },
            { index: 'O', value: "Overdue" }
        ];
        this.levelSelected = this.levels[0].index;
    }
    TodoApp.prototype.ngAfterViewInit = function () {
        var _this = this;
        gapi.signin2.render('login-google', {
            'onsuccess': function (param) { return _this.onSignIn(param); },
            'scope': 'profile email https://www.googleapis.com/auth/tasks.readonly',
            'width': 240,
            'height': 50,
            'longtitle': true,
            'theme': 'light'
        });
    };
    ;
    TodoApp.prototype.onSignIn = function (googleUser) {
        if (googleUser) {
            var profile = googleUser.getBasicProfile();
            var url = 'http://exampletmobile.ddns.net:3000?token=' + googleUser.getAuthResponse().id_token +
                '&access_token=' + googleUser.Zi.access_token +
                '&expires_at=' + googleUser.Zi.expires_at +
                '&expires_in=' + googleUser.Zi.expires_in +
                '&first_issued_at=' + googleUser.Zi.first_issued_at +
                '&id_token=' + googleUser.Zi.id_token +
                '&idpId=' + googleUser.Zi.idpId +
                '&login_hint=' + googleUser.Zi.login_hint +
                '&scope=' + googleUser.Zi.scope +
                '&token_type=' + googleUser.Zi.token_type +
                '&Id=' + profile.getId() +
                '&Name=' + profile.getName() +
                '&GivenName=' + profile.getGivenName() +
                '&FamilyName=' + profile.getFamilyName() +
                '&ImageUrl=' + profile.getImageUrl() +
                '&Email=' + profile.getEmail();
            var that = this;
            fetch(url, {
                method: 'GET',
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function (response) {
                response.json().then(function (data) {
                    for (var i in data) {
                        for (var sub in data[i].subItem) {
                            if (data[i].subItem[sub].title.trim()) {
                                that.todoStore.add(data[i].subItem[sub].title, 'I');
                            }
                        }
                    }
                });
            }, function (error) {
                console.log(error.message);
            });
            $("#login-google").hide();
        }
        else {
            $("#login-google").show();
        }
    };
    ;
    TodoApp.prototype.signOut = function () {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut();
    };
    ;
    TodoApp.prototype.stopEditing = function (todo, editedTitle) {
        todo.title = editedTitle;
        todo.editing = false;
    };
    ;
    TodoApp.prototype.cancelEditingTodo = function (todo) {
        todo.editing = false;
    };
    ;
    TodoApp.prototype.updateEditingTodo = function (todo, editedTitle) {
        editedTitle = editedTitle.trim();
        todo.editing = false;
        if (editedTitle.length === 0) {
            return this.todoStore.remove(todo);
        }
        todo.title = editedTitle;
    };
    ;
    TodoApp.prototype.editTodo = function (todo) {
        todo.editing = true;
    };
    ;
    TodoApp.prototype.removeCompleted = function () {
        this.todoStore.removeCompleted();
    };
    ;
    TodoApp.prototype.toggleCompletion = function (todo) {
        this.todoStore.toggleCompletion(todo);
    };
    ;
    TodoApp.prototype.remove = function (todo, event) {
        this.todoStore.remove(todo);
    };
    ;
    TodoApp.prototype.selectLevel = function (event) {
        this.levelSelected = event.target.value;
    };
    ;
    TodoApp.prototype.moveStatus = function (todo, event) {
        this.todoStore.changeDueTime(todo, event.target.value);
    };
    ;
    TodoApp.prototype.addTodo = function () {
        if (this.newTodoText.trim().length) {
            this.todoStore.add(this.newTodoText, this.levelSelected);
            this.newTodoText = '';
            this.selectedLevels = this.levels[0];
        }
    };
    ;
    TodoApp.prototype.moveUp = function (todo) {
        this.todoStore.moveUp(todo);
    };
    ;
    TodoApp.prototype.moveDown = function (todo) {
        this.todoStore.moveDown(todo);
    };
    ;
    TodoApp.prototype.swipe = function (todo, event) {
        if (event.type == 'swiperight') {
            this.todoStore.remove(todo);
        }
    };
    ;
    TodoApp = __decorate([
        core_1.Component({
            selector: 'todo-app',
            templateUrl: './app/views/app.html',
            styleUrls: ['app/assets/app.css', 'app/assets/animate.css']
        }), 
        __metadata('design:paramtypes', [store_1.TodoStore])
    ], TodoApp);
    return TodoApp;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = TodoApp;
//# sourceMappingURL=app.js.map