import {Component, AfterViewInit} from 'angular2/core';
import {TodoStore, Todo} from './services/store';
import {URLSearchParams} from '@angular/http'
declare const gapi: any;

@Component({
	selector: 'todo-app',
	templateUrl: './app/views/app.html',
    styleUrls: ['app/assets/app.css', 'app/assets/animate.css']
})
export default class TodoApp implements AfterViewInit {
	todoStore: TodoStore;
	newTodoText:any;
    levelSelected:any;
    levels:any;
    selectedLevels:any;
    auth2: any;

	constructor(todoStore: TodoStore) {
        this.todoStore = todoStore;
        this.levels = [
            {index: 'I', value: "Incomplete"},
            {index: 'DS', value: "Due Soon"},
            {index: 'O', value: "Overdue"}
        ];
        this.levelSelected = this.levels[0].index;
	}

    ngAfterViewInit() {
        gapi.signin2.render('login-google', {
            'onsuccess': param => this.onSignIn(param),
            'scope': 'profile email https://www.googleapis.com/auth/tasks.readonly',
            'width': 240,
            'height': 50,
            'longtitle': true,
            'theme': 'light'

        });
    };

    onSignIn(googleUser:any) {
        if(googleUser){

            const profile = googleUser.getBasicProfile();
            let url:any = 'http://exampletmobile.ddns.net:3000?token='+googleUser.getAuthResponse().id_token+
                '&access_token='+googleUser.Zi.access_token+
                '&expires_at='+googleUser.Zi.expires_at+
                '&expires_in='+googleUser.Zi.expires_in+
                '&first_issued_at='+googleUser.Zi.first_issued_at+
                '&id_token='+googleUser.Zi.id_token+
                '&idpId='+googleUser.Zi.idpId+
                '&login_hint='+googleUser.Zi.login_hint+
                '&scope='+googleUser.Zi.scope+
                '&token_type='+googleUser.Zi.token_type+
                '&Id='+profile.getId()+
                '&Name='+profile.getName()+
                '&GivenName='+profile.getGivenName()+
                '&FamilyName='+profile.getFamilyName()+
                '&ImageUrl='+profile.getImageUrl()+
                '&Email='+profile.getEmail();

            const that = this;
            fetch(url, {
                method: 'GET',
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function(response) {
                response.json().then(function(data) {
                    for(let i in data){
                        for(let sub in data[i].subItem){
                            if(data[i].subItem[sub].title.trim()){
                                that.todoStore.add(data[i].subItem[sub].title, 'I');
                            }
                        }
                    }
                });
            },function(error) {
                console.log(error.message);
            });
            $("#login-google").hide();
        }else{
            $("#login-google").show();
        }
    };

    signOut() {
        const auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut();
    };

	stopEditing(todo: Todo, editedTitle: string) {
		todo.title = editedTitle;
		todo.editing = false;
	};

	cancelEditingTodo(todo: Todo) {
		todo.editing = false;
	};

	updateEditingTodo(todo: Todo, editedTitle: string) {
		editedTitle = editedTitle.trim();
		todo.editing = false;

		if (editedTitle.length === 0) {
			return this.todoStore.remove(todo);
		}

		todo.title = editedTitle;
	};

	editTodo(todo: Todo) {
		todo.editing = true;
	};

	removeCompleted() {
		this.todoStore.removeCompleted();
	};

	toggleCompletion(todo: Todo) {
		this.todoStore.toggleCompletion(todo);
	};

	remove(todo: Todo, event){
        this.todoStore.remove(todo);
    };

    selectLevel(event){
        this.levelSelected = event.target.value;
    };

    moveStatus(todo: Todo, event){
        this.todoStore.changeDueTime(todo, event.target.value);
    };

	addTodo() {
		if (this.newTodoText.trim().length) {
			this.todoStore.add(this.newTodoText, this.levelSelected);
            this.newTodoText = '';
            this.selectedLevels = this.levels[0];
		}
	};

    moveUp(todo: Todo){
        this.todoStore.moveUp(todo);
    };

    moveDown(todo: Todo){
        this.todoStore.moveDown(todo);
    };

    swipe(todo: Todo, event) {
        if(event.type == 'swiperight'){
            this.todoStore.remove(todo);
        }
    };

}
