# Test T Mobile

Thank you for taking the time to interview with us.
We think you had some great answers to our questions, and we’d like to move
forward with the interview process in order to get a better understanding of your
technical skills.
We would like to invite you to participate in this coding challenge! This will help
us evaluate and see your technical skills in action. You will be given one week to
complete this challenge.
On the next page, you will find all of the instructions you will need in order to
complete the challenge.
The Coding Challenge: Instructions & Resources


Resources:
• To-Do Angular2 App - Source Code
https://github.com/tastejs/todomvc/tree/master/examples/angular2
• To-Do Angular2 App - Live Demo
http://todomvc.com/examples/angular2/
Getting Started:
Please fork the GitHub repository for the To-Do Angular2 App
(https://github.com/tastejs/todomvc/tree/master/examples/angular2).
This code base will be your starting point.
You will be adding functionality and styling to this baseline project for your coding challenge.
What we would like you to do with this baseline code base:
1. Add “Due Dates” to each task item in the To-Do list
This feature will allow users to specify a date that each task in the to-do list needs to be
completed by. Use some styling to draw attention to tasks that are incomplete and are
a) due soon or b) overdue.
2. Add “Priority” to each task item in the To-Do list
This feature will allow the user to rank the priority of their tasks within the to-do list.
You have the freedom to decide how the priority will be ranked.
3. Edit styling of the page to match T-Mobile branding
You can do this however you like, and it does not need to be exactly right. We just
want to see what you come up with while using the correct colors, themes, logos, etc.
Please refer to the provided the T-Mobile_Branding.pdf file for branding colors and
typography. You may refer to these webpages to see some brand/styling elements used
in a web environment:
a. www.t-mobile.com
b. https://support.t-mobile.com/welcome
c. https://uncarrier.t-mobile.com/premium-device-protection-plans
4. Please track all your changes in Git.
We would like to see a proper Git commit history with meaningful commit messages
and frequent commits. When you are finished, please submit your Git repo link to
Ganesh for review along with any instructions to run your project if you change any of
those configurations.
5. BONUS: Incorporate the Google Tasks API.
Use this by creating a Google Tasks list, and using the API to populate your To-Do
Angular2 App with the tasks from your Google Tasks list. This is optional. If you have
finished the other portions of the challenge and want to further show your technical
skills, this is a good way to do it!
a. Google Tasks API Link
https://developers.google.com/google-apps/tasks/?csw=1
Please let us know if you have any questions.


# How it works!
The project have 2 part the first part is in the repo (frontend):
https://CaAllianceIT@bitbucket.org/CaAllianceIT/ustglobal.git
And the secod part is in the repo (backend):
https://CaAllianceIT@bitbucket.org/CaAllianceIT/ustglobal-server.git


# How we have to do!

  - I have to created a DNS we choosed https://www.noip.com/login?ref_url=console
  - Our url is http://exampletmobile.ddns.net/
  - I choosed this one because is free, and is neccesary for the API Google 
  - Dev Server, we use a server http://199.231.160.212/
  - I created an application from the requirement, also i had to created a backend for the application

# Each branch have one of the required todos
  - All is in master

###### Luis Miguel Sanchez Calderon

